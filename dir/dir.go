// Copyright © 2018 Charles Haynes <ceh@ceh.bz>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package dir

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/dhowden/tag"
)

// Entry is a directory entry. Dir is the path of the containing directory
// Name is the name of the entry, Info is the os.FileInfo of the entry
type Entry struct {
	Dir  string
	Name string
	Info os.FileInfo
}

func (e Entry) walkdir(ch chan<- Entry) error {
	path := filepath.Join(e.Dir, e.Name)
	names, err := readDirNames(path)
	if err != nil {
		return err
	}

	for _, name := range names {
		filename := filepath.Join(path, name)
		fileInfo, err := os.Lstat(filename)
		if err != nil {
			return err
		}
		en := Entry{path, name, fileInfo}
		err = en.walk(ch)
		if err != nil {
			return err
		}
	}

	return nil
}

func (e Entry) walk(ch chan<- Entry) error {
	if e.Info.IsDir() {
		if err := e.walkdir(ch); err != nil {
			return err
		}
	}

	// ss := e.Info.Sys().(*syscall.Stat_t)
	ch <- e
	return nil
}

// readDirNames reads the directory named by dirname and returns
// an unsorted list of directory entries.
func readDirNames(dirname string) (names []string, err error) {
	if f, err := os.Open(dirname); err == nil {
		names, err = f.Readdirnames(-1)
		f.Close()
	}
	return
}

// Spec is an Artist, Album, and Format
type Spec struct {
	ArtistAlbumStruct
	Format string
}

// NewSpec returns the artist, album, and formatof a music file
// or an error if it is not a music file or there are no artist
// album, or format information
func NewSpec(fn string) (Spec, error) {
	f, err := os.Open(fn)
	if err != nil {
		return Spec{}, err
	}
	defer f.Close()
	m, err := tag.ReadFrom(f)
	if err != nil {
		return Spec{}, err
	}
	if m.Artist() == "" {
		return Spec{}, fmt.Errorf("missing artist")
	}
	if m.Album() == "" {
		return Spec{}, fmt.Errorf("missing album")
	}
	if m.FileType() == "" {
		return Spec{}, fmt.Errorf("missing file type")
	}
	return Spec{ArtistAlbumStruct{m.Artist(), m.Album()}, string(m.FileType())}, nil
}

// ArtistAlbumStruct is a single album by a single artist
type ArtistAlbumStruct struct{ Artist, Album string }

// TorrentDir is an artist, album, and directory that might
// be torrentable
type TorrentDir struct {
	Spec
	Dir string
}

// TorrentNameDir is a log filename, and directory that might
// be torrentable
type TorrentNameDir struct {
	Name string
	Dir  string
}

// FindTorrentableNames walks a directory tree and emits all
// the potentially torrentable directories and the logfile name
func FindTorrentableNames(root string, tc chan<- TorrentNameDir) {
	ch := make(chan Entry)
	go Enumerate(root, ch)
	for {
		e, ok := <-ch
		if !ok {
			break
		}
		if !e.Info.IsDir() && filepath.Ext(e.Name) == ".log" {
			tc <- TorrentNameDir{e.Name, e.Dir}
		}
	}
	close(tc)
}

// FindTorrentableSpecs walks a directory tree and emits all
// the potentially torrentable directories, their artist, and album
// it emits each distinct Spec/Directory tuple only once
func FindTorrentableSpecs(root string, tc chan<- TorrentDir) {
	dirs := map[string]map[Spec]bool{}
	ch := make(chan Entry)
	go Enumerate(root, ch)
	for {
		e, ok := <-ch
		if !ok {
			break
		}
		path := filepath.Join(e.Dir, e.Name)
		if e.Info.IsDir() {
			for s := range dirs[path] {
				tc <- TorrentDir{s, path}
			}
			continue
		}
		s, err := NewSpec(path)
		if err != nil {
			continue
		}
		if dirs[e.Dir] == nil {
			dirs[e.Dir] = make(map[Spec]bool)
		}
		dirs[e.Dir][s] = true
	}
	close(tc)
}

// Enumerate enumerates all directories in a path
// that have files and sends their entries to the channel ch
func Enumerate(root string, ch chan<- Entry) {
	defer close(ch)
	info, err := os.Lstat(root)
	if err != nil {
		return
	}
	d, n := filepath.Split(root)
	en := Entry{d, n, info}
	err = en.walk(ch)
	if err != nil {
		log.Fatalf("Enumerate: error %s", err)
	}
}

// Copyright © 2018 Charles Haynes <ceh@ceh.bz>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package dir_test

import (
	"log"
	"os"
	"path/filepath"
	"reflect"
	"testing"

	"github.com/charles-haynes/upload/dir"
)

func TestEnumeratesAllFiles(t *testing.T) {
	exp := map[string]interface{}{
		"testdata":                                 nil,
		"testdata/empty":                           nil,
		"testdata/taggedaudio":                     nil,
		"testdata/taggedaudio/tagged.mp3":          nil,
		"testdata/taggedaudio2":                    nil,
		"testdata/taggedaudio2/tagged2.mp3":        nil,
		"testdata/taggedaudio2/tagged3.mp3":        nil,
		"testdata/taggedaudio2/tagged4.mp3":        nil,
		"testdata/nonaudio":                        nil,
		"testdata/nonaudio/nonaudio.txt":           nil,
		"testdata/untaggedaudio":                   nil,
		"testdata/untaggedaudio/noalbum.mp3":       nil,
		"testdata/untaggedaudio/noartist.mp3":      nil,
		"testdata/untaggedaudio/untaggedaudio.mp3": nil,
	}

	if _, err := os.Lstat("testdata/empty"); os.IsNotExist(err) {
		err = os.Mkdir("testdata/empty", 0755)
		if err != nil {
			log.Fatalf(`Can't create "testdata/empty": %s`, err)
		}
	}
	ents := map[string]dir.Entry{}
	ch := make(chan dir.Entry)
	go dir.Enumerate("testdata", ch)

	// record everything that comes back
	for {
		ent, ok := <-ch
		if !ok {
			break
		}
		path := filepath.Join(ent.Dir, ent.Name)
		// no dups
		if _, ok := ents[path]; ok {
			t.Errorf("duplicate %s", path)
		}
		// dirs come after all their entries
		if _, ok := ents[ent.Dir]; ok {
			t.Errorf("already seen dir %s", ent.Dir)
		}
		ents[path] = ent
	}

	// get everything expected
	for d := range exp {
		if _, ok := ents[d]; !ok {
			t.Errorf("expected to get %s", d)
		}
	}

	// don't get anything unexpected
	for d := range ents {
		if _, ok := exp[d]; !ok {
			t.Errorf("did not expect to get %s", d)
		}
	}
}

func TestBuildDirMap(t *testing.T) {
	exp := map[dir.ArtistAlbumStruct]map[string]interface{}{
		dir.ArtistAlbumStruct{"artist", "album"}: {
			"testdata/taggedaudio":  nil,
			"testdata/taggedaudio2": nil,
		},
		dir.ArtistAlbumStruct{"artist2", "album2"}: {
			"testdata/taggedaudio2": nil,
		},
	}

	aas := map[dir.ArtistAlbumStruct]map[string]interface{}{}

	ch := make(chan dir.TorrentDir)
	go dir.FindTorrentable("testdata", ch)
	for {
		td, ok := <-ch
		if !ok {
			break
		}
		if v, ok := aas[td.ArtistAlbumStruct]; ok {
			if _, ok := v[td.Dir]; ok {
				t.Errorf("FindTorrentable duplicate %s", td)
			}
		} else {
			aas[td.ArtistAlbumStruct] = map[string]interface{}{}
		}
		aas[td.ArtistAlbumStruct][td.Dir] = nil
	}

	if len(aas) != len(exp) {
		t.Errorf("Expected %d ents, got %d", len(exp), len(aas))
	}

	for k, v := range exp {
		if _, ok := aas[k]; !ok {
			t.Errorf("expected result to contain %s", k)
		}
		if len(v) != len(aas[k]) {
			t.Errorf("expected %s to have %d ents, got %d",
				k, len(v), len(aas[k]))
		}
		for d := range v {
			if _, ok := aas[k][d]; !ok {
				t.Errorf("expected %s to contain %s", k, d)
			}
		}
	}
}

func TestArtistAlbum(t *testing.T) {
	s1 := dir.Spec{dir.ArtistAlbumStruct{"artist", "album"}, "MP3"}
	s2 := dir.Spec{dir.ArtistAlbumStruct{"artist2", "album2"}, "MP3"}
	tests := []struct {
		path string
		spec dir.Spec
		err  bool
	}{
		{"testdata/taggedaudio/tagged.mp3", s1, false},
		{"testdata/taggedaudio2/tagged2.mp3", s2, false},
		{"testdata/taggedaudio2/tagged3.mp3", s1, false},
		{"testdata/taggedaudio2/tagged4.mp3", s2, false},
		{"testdata/nonaudio/nonaudio.txt", dir.Spec{}, true},
		{"testdata/untaggedaudio/noalbum.mp3", dir.Spec{}, true},
		{"testdata/untaggedaudio/noartist.mp3", dir.Spec{}, true},
		{"testdata/untaggedaudio/untaggedaudio.mp3", dir.Spec{}, true},
	}

	for _, test := range tests {
		s, err := dir.NewSpec(test.path)
		if (err != nil) != test.err {
			t.Errorf("\"%s\": expected err %t got %t",
				test.path, test.err, err != nil)
			continue
		}
		if !reflect.DeepEqual(s, test.spec) {
			t.Errorf(`"%s": expected spec %q, got %q`, test.path, test.spec, s)
		}
	}
}

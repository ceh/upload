module gitlab.com/ceh/upload

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/RoaringBitmap/roaring v0.4.16 // indirect
	github.com/anacrolix/dht v1.0.0 // indirect
	github.com/anacrolix/log v0.2.0
	github.com/anacrolix/missinggo v0.1.0 // indirect
	github.com/anacrolix/mmsg v0.0.0-20180808012353-5adb2c1127c0 // indirect
	github.com/anacrolix/torrent v1.0.1
	github.com/anacrolix/utp v0.0.0-20180219060659-9e0e1d1d0572 // indirect
	github.com/charles-haynes/whatapi v0.0.0-20181122220552-b8267a4a8dab
	github.com/coreos/etcd v3.3.12+incompatible // indirect
	github.com/cosiner/argv v0.0.1 // indirect
	github.com/dhowden/tag v0.0.0-20181104225729-a9f04c2798ca
	github.com/glycerine/go-unsnap-stream v0.0.0-20181221182339-f9677308dec2 // indirect
	github.com/glycerine/goconvey v0.0.0-20180728074245-46e3a41ad493 // indirect
	github.com/go-delve/delve v1.2.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181103185306-d547d1d9531e // indirect
	github.com/huandu/xstrings v1.2.0 // indirect
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kr/pty v1.1.3 // indirect
	github.com/mattn/go-colorable v0.1.0 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mschoch/smat v0.0.0-20160514031455-90eadee771ae // indirect
	github.com/peterh/liner v1.1.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pkg/profile v1.2.1 // indirect
	github.com/rogpeppe/godef v1.1.1 // indirect
	github.com/sirupsen/logrus v1.3.0 // indirect
	github.com/smartystreets/assertions v0.0.0-20190215210624-980c5ac6f3ac // indirect
	github.com/smartystreets/goconvey v0.0.0-20181108003508-044398e4856c // indirect
	github.com/smartystreets/gunit v0.0.0-20180314194857-6f0d6275bdcd // indirect
	github.com/spf13/afero v1.2.1 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	github.com/stamblerre/gocode v0.0.0-20190213022308-8cc90faaf476 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/tinylib/msgp v1.1.0 // indirect
	github.com/ugorji/go/codec v0.0.0-20190204201341-e444a5086c43 // indirect
	github.com/willf/bitset v1.1.10 // indirect
	github.com/willf/bloom v2.0.3+incompatible // indirect
	golang.org/x/arch v0.0.0-20181203225421-5a4828bb7045 // indirect
	golang.org/x/crypto v0.0.0-20190219172222-a4c6cb3142f2 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
	golang.org/x/sys v0.0.0-20190220154126-629670e5acc5 // indirect
	golang.org/x/tools v0.0.0-20190221000707-a754db16a40a // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

// Copyright © 2018 Charles Haynes <ceh@ceh.bz>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package torrent_test

import (
	"fmt"
	"math"
	"net/url"
	"testing"

	"github.com/anacrolix/torrent/metainfo"
	"github.com/charles-haynes/seed/dir"
	"github.com/charles-haynes/seed/torrent"
	"github.com/charles-haynes/whatapi"
)

type testAPI struct{}

func (testAPI) GetJSON(requestURL string, responseObj interface{}) (err error)          { return }
func (testAPI) Do(action string, params url.Values, result interface{}) (err error)     { return }
func (testAPI) CreateDownloadURL(id int) (s string, err error)                          { return }
func (testAPI) Login(username, password string) (err error)                             { return }
func (testAPI) Logout() (err error)                                                     { return }
func (testAPI) GetAccount() (r whatapi.Account, err error)                              { return }
func (testAPI) GetMailbox(params url.Values) (r whatapi.Mailbox, err error)             { return }
func (testAPI) GetConversation(id int) (r whatapi.Conversation, err error)              { return }
func (testAPI) GetNotifications(params url.Values) (r whatapi.Notifications, err error) { return }
func (testAPI) GetAnnouncements() (r whatapi.Announcements, err error)                  { return }
func (testAPI) GetSubscriptions(params url.Values) (r whatapi.Subscriptions, err error) { return }
func (testAPI) GetCategories() (r whatapi.Categories, err error)                        { return }
func (testAPI) GetForum(id int, params url.Values) (r whatapi.Forum, err error)         { return }
func (testAPI) GetThread(id int, params url.Values) (r whatapi.Thread, err error)       { return }
func (testAPI) GetArtistBookmarks() (r whatapi.ArtistBookmarks, err error)              { return }
func (testAPI) GetTorrentBookmarks() (r whatapi.TorrentBookmarks, err error)            { return }
func (testAPI) GetArtist(id int, params url.Values) (r whatapi.Artist, err error)       { return }
func (testAPI) GetRequest(id int, params url.Values) (r whatapi.Request, err error)     { return }
func (testAPI) GetTorrent(id int, params url.Values) (whatapi.Torrent, error) {
	torrents := map[int]whatapi.Torrent{
		7590: whatapi.Torrent{
			Torrent: whatapi.TorrentType{
				ID:       7590,
				Size:     0,
				FilePath: "album",
				FileList: "empty{{{0}}}",
			},
		},
		11643: whatapi.Torrent{
			Torrent: whatapi.TorrentType{
				ID:       11643,
				Size:     111 + 222 + 333,
				FilePath: "torrent11643",
				FileList: "aaa.mp3{{{111}}}|||bbb.mp3{{{222}}}|||ccc.mp3{{{333}}}",
			},
		},
		19628: whatapi.Torrent{
			Torrent: whatapi.TorrentType{
				ID:       19628,
				Size:     1000000 + 1340,
				FilePath: "torrent19628",
				FileList: "file.mp3{{{1340}}}|||file3.mp3{{{1000000}}}",
			},
		},
		26432: whatapi.Torrent{
			Torrent: whatapi.TorrentType{
				ID:       26432,
				Size:     1340 + 1340,
				FilePath: "torrent26432",
				FileList: "ddd/file1.mp3{{{1340}}}|||ddd/file4.mp3{{{1340}}}",
			},
		},
		26433: whatapi.Torrent{
			Torrent: whatapi.TorrentType{
				ID:       26433,
				Size:     1340 + 1340,
				FilePath: "album2",
				FileList: "file5.mp3{{{1340}}}|||ddd/file6.mp3{{{1340}}}",
			},
		},
		26434: whatapi.Torrent{
			Torrent: whatapi.TorrentType{
				ID:       26434,
				Size:     1340 + 1340,
				FilePath: "torrent26&#52;34",
				FileList: "file&#53;.mp3{{{1340}}}|||ddd/file6.mp3{{{1340}}}",
			},
		},
		26435: whatapi.Torrent{
			Torrent: whatapi.TorrentType{
				ID:       26435,
				Size:     1340 + 1340,
				FilePath: "album2",
				FileList: "file4.mp3{{{1340}}}|||eee/file.mp3{{{1340}}}",
			},
		},
		26436: whatapi.Torrent{
			Torrent: whatapi.TorrentType{
				ID:       26436,
				Size:     1340 + 1340,
				FilePath: "album2",
				FileList: "file4.mp3{{{1340}}}|||ddd/file6.mp3{{{1340}}}",
			},
		},
		26437: whatapi.Torrent{
			Torrent: whatapi.TorrentType{
				ID:       26437,
				Size:     1340 + 1340,
				FilePath: "album2",
				FileList: "file4.mp3{{{1340}}}|||eee/file1.mp3{{{1340}}}",
			},
		},
		26438: whatapi.Torrent{
			Torrent: whatapi.TorrentType{
				ID:       26438,
				Size:     1340 + 1340,
				FilePath: "torrent26438",
				FileList: "file4.mp3{{{1340}}}|||ddd/file.mp3{{{1340}}}",
			},
		},
		60616: whatapi.Torrent{Torrent: whatapi.TorrentType{ID: 60616}},
		60282: whatapi.Torrent{Torrent: whatapi.TorrentType{ID: 60282}},
	}
	if r, ok := torrents[id]; ok {
		return r, nil
	}
	return whatapi.Torrent{}, fmt.Errorf("no such torrent %d", id)
}
func (testAPI) GetTorrentGroup(id int, params url.Values) (r whatapi.TorrentGroup, err error) { return }
func (testAPI) SearchTorrents(searchStr string, params url.Values) (r whatapi.TorrentSearch, err error) {
	res := map[dir.Spec]whatapi.TorrentSearch{
		dir.Spec{dir.ArtistAlbumStruct{"artist", "album"}, "flac"}: {
			Results: []whatapi.TorrentSearchResultStruct{whatapi.TorrentSearchResultStruct{
				Torrents: []whatapi.TorrentStruct{
					whatapi.TorrentStruct{TorrentID: 60616},
					whatapi.TorrentStruct{TorrentID: 60282},
				},
			},
			},
		},
	}
	t := dir.Spec{}
	if p, ok := params["artistname"]; ok {
		t.Artist = p[0]
	}
	if p, ok := params["groupname"]; ok {
		t.Album = p[0]
	}
	if p, ok := params["format"]; ok {
		t.Format = p[0]
	}
	if r, ok := res[t]; ok {
		return r, nil
	}
	return r, fmt.Errorf("not found %+v", t)
}
func (testAPI) SearchRequests(searchStr string, params url.Values) (r whatapi.RequestsSearch, err error) {
	return
}
func (testAPI) SearchUsers(searchStr string, params url.Values) (r whatapi.UserSearch, err error) {
	return
}
func (testAPI) GetTopTenTorrents(params url.Values) (r whatapi.TopTenTorrents, err error) { return }
func (testAPI) GetTopTenTags(params url.Values) (r whatapi.TopTenTags, err error)         { return }
func (testAPI) GetTopTenUsers(params url.Values) (r whatapi.TopTenUsers, err error)       { return }
func (testAPI) GetSimilarArtists(id, limit int) (r whatapi.SimilarArtists, err error)     { return }

var testapi = testAPI{}

var finder = torrent.GazelleFinder{testapi}

func TestFind(t *testing.T) {
	exp := map[int]interface{}{
		60616: nil,
		60282: nil,
	}
	torrents := map[int]interface{}{}
	ch := make(chan whatapi.Torrent)
	go finder.Find(
		dir.Spec{
			dir.ArtistAlbumStruct{"artist", "album"}, "flac"},
		ch)
	for {
		to, ok := <-ch
		if !ok {
			break
		}
		if _, ok := torrents[to.Torrent.ID]; ok {
			t.Errorf("duplicate %d", to.Torrent.ID)
		}
		torrents[to.Torrent.ID] = nil
	}
	for to := range exp {
		if _, ok := torrents[to]; !ok {
			t.Errorf("missing %d", to)
		}
	}
	for to := range torrents {
		if _, ok := exp[to]; !ok {
			t.Errorf("unexpected %d", to)
		}
	}
}

func equalChange(a, b []torrent.Change) bool {
	if len(a) != len(b) {
		return false
	}
	m := map[string]string{}
	for _, c := range a {
		m[c.From] = c.To
	}
	for _, c := range b {
		if m[c.From] != c.To {
			return false
		}
	}
	return true
}

func TestCompare(t *testing.T) {
	var (
		to  whatapi.Torrent
		p   float32
		s   int64
		c   []torrent.Change
		err error
	)

	exp := map[int]struct {
		Dir string
		P   float64
		S   int64
		C   []torrent.Change
	}{
		// empty torrent (zero size) always matches
		// torrent name matches dir, files match, no changes needed
		7590: {
			Dir: "testdata/artist/album",
			P:   1.0,
			S:   0,
			C:   []torrent.Change{},
		},
		// torrent has no files in common, percent 0, size is total size of torrent
		// torrent name doesn't match dir, no files match, change dir only
		11643: {
			Dir: "testdata/artist/album",
			P:   0.0,
			S:   111 + 222 + 333,
			C: []torrent.Change{
				{From: "testdata/artist/album", To: "testdata/artist/torrent11643"},
			},
		},
		// torrent has one file out of two in common, percent != 0, size is size of missing file
		// torrent name doesn't match dir, files match, change dir only
		19628: {
			Dir: "testdata/artist/album",
			P:   float64(1340.0 / 1000000),
			S:   1000000,
			C: []torrent.Change{
				{From: "testdata/artist/album", To: "testdata/artist/torrent19628"},
			},
		},
		// torrent has two files out of two in common, percent = 1.0, size is 0
		// one file has right name but not in subdir, rename to subdir
		// one file is in subdir but wrong name, rename to correct name
		26432: {
			Dir: "testdata/artist/album2",
			P:   1.0,
			S:   0,
			C: []torrent.Change{
				{From: "testdata/artist/album2", To: "testdata/artist/torrent26432"},
				{From: "ddd/file.mp3", To: "ddd/file4.mp3"},
				{From: "file4.mp3", To: "ddd/file1.mp3"},
			},
		},
		// torrent name matches dir, files don't match, change files only
		26433: {
			Dir: "testdata/artist/album2",
			P:   1.0,
			S:   0,
			C: []torrent.Change{
				{From: "ddd/file.mp3", To: "ddd/file6.mp3"},
				{From: "file4.mp3", To: "file5.mp3"},
			},
		},
		// torrent name doesn't match, files don't match, change dir and files
		// do html decode on paths and file names
		26434: {
			Dir: "testdata/artist/album2",
			P:   1.0,
			S:   0,
			C: []torrent.Change{
				{From: "testdata/artist/album2", To: "testdata/artist/torrent26434"},
				{From: "file4.mp3", To: "file5.mp3"},
				{From: "ddd/file.mp3", To: "ddd/file6.mp3"},
			},
		},
		// torrent name matches, file names match, but subdir doesn't match, change subdir name
		26435: {
			Dir: "testdata/artist/album2",
			P:   1.0,
			S:   0,
			C: []torrent.Change{
				{From: "ddd/file.mp3", To: "eee/file.mp3"},
			},
		},
		// torrent name matches, subdir matches, file name doesn't match, change file name
		26436: {
			Dir: "testdata/artist/album2",
			P:   1.0,
			S:   0,
			C: []torrent.Change{
				{From: "ddd/file.mp3", To: "ddd/file6.mp3"},
			},
		},
		// torrent name matches, subdir doesn't match file name doesn't match, change subdir and file
		26437: {
			Dir: "testdata/artist/album2",
			P:   1.0,
			S:   0,
			C: []torrent.Change{
				{From: "ddd/file.mp3", To: "eee/file1.mp3"},
			},
		},
		// torrent name doesn't match, subdir matches file name matches, just change torrent dir
		26438: {
			Dir: "testdata/artist/album2",
			P:   1.0,
			S:   0,
			C: []torrent.Change{
				{From: "testdata/artist/album2", To: "testdata/artist/torrent26438"},
			},
		},
	}

	for i, e := range exp {
		to, err = testapi.GetTorrent(i, nil)
		if err != nil {
			t.Fatalf("GetTorrent(%d) failed: %s", i, err)
		}
		to, err = testapi.GetTorrent(i, nil)
		p, s, c = torrent.Compare(to, e.Dir)
		if e.P != float64(p) && (e.P == 0 || math.Abs(e.P-float64(p))/e.P > 0.01) {
			t.Errorf("Compare(%d, %s) failed: expected p %f, got %f (%f > 0.01)", i, e.Dir, e.P, p, math.Abs(e.P-float64(p))/e.P)
		}
		if e.S != s {
			t.Errorf("Compare(%d, %s) failed: expected s %d, got %d", i, e.Dir, e.S, s)
		}
		if !equalChange(e.C, c) {
			t.Errorf("Compare(%d, %s) failed: expected c %+v, got %+v", i, e.Dir, e.C, c)
		}
	}
}

func TestCompareInfo(t *testing.T) {
	testData := []struct {
		info metainfo.Info
		dir  string
		p    float64
		s    int64
		c    []torrent.Change
	}{
		{
			info: metainfo.Info{
				Name:   "info",
				Length: 1340,
				Files: []metainfo.FileInfo{
					{Length: 1340, Path: []string{"file.mp3"}},
				},
			},
			dir: "testdata/artist/album",
			p:   1.0,
			s:   0,
			c: []torrent.Change{
				{From: "testdata/artist/album", To: "testdata/artist/info"},
			},
		},
		// empty torrent (zero size) always matches
		// torrent name matches dir, files match, no changes needed
		{
			info: metainfo.Info{
				Length: 0,
				Name:   "album",
				Files: []metainfo.FileInfo{
					{Length: 0, Path: []string{"empty"}},
				},
			},
			dir: "testdata/artist/album",
			p:   1.0,
			s:   0,
			c:   []torrent.Change{},
		},
		// torrent has no files in common, percent 0, size is total size of torrent
		// torrent name doesn't match dir, no files match, change dir only
		{
			info: metainfo.Info{
				Length: 111 + 222 + 333,
				Name:   "info11643",
				Files: []metainfo.FileInfo{
					{Length: 111, Path: []string{"aaa.mp3"}},
					{Length: 222, Path: []string{"bbb.mp3"}},
					{Length: 333, Path: []string{"ccc.mp3"}},
				},
			},
			dir: "testdata/artist/album",
			p:   0.0,
			s:   111 + 222 + 333,
			c: []torrent.Change{
				{From: "testdata/artist/album", To: "testdata/artist/info11643"},
			},
		},
		// torrent has one file out of two in common, percent != 0, size is size of missing file
		// torrent name doesn't match dir, files match, change dir only
		{
			info: metainfo.Info{
				Length: 1000000 + 1340,
				Name:   "info19628",
				Files: []metainfo.FileInfo{
					{Length: 1340, Path: []string{"file.mp3"}},
					{Length: 1000000, Path: []string{"file3.mp3"}},
				},
			},
			dir: "testdata/artist/album",
			p:   float64(1340.0 / 1000000),
			s:   1000000,
			c: []torrent.Change{
				{From: "testdata/artist/album", To: "testdata/artist/info19628"},
			},
		},
		// torrent has two files out of two in common, percent = 1.0, size is 0
		// one file has right name but not in subdir, rename to subdir
		// one file is in subdir but wrong name, rename to correct name
		{
			info: metainfo.Info{
				Length: 1340 + 1340,
				Name:   "info26432",
				Files: []metainfo.FileInfo{
					{Length: 1340, Path: []string{"ddd", "file1.mp3"}},
					{Length: 1340, Path: []string{"ddd", "file4.mp3"}},
				},
			},
			dir: "testdata/artist/album2",
			p:   1.0,
			s:   0,
			c: []torrent.Change{
				{From: "testdata/artist/album2", To: "testdata/artist/info26432"},
				{From: "ddd/file.mp3", To: "ddd/file4.mp3"},
				{From: "file4.mp3", To: "ddd/file1.mp3"},
			},
		},
		// torrent name matches dir, files don't match, change files only
		{
			info: metainfo.Info{
				Length: 1340 + 1340,
				Name:   "album2",
				Files: []metainfo.FileInfo{
					{Length: 1340, Path: []string{"file5.mp3"}},
					{Length: 1340, Path: []string{"ddd", "file6.mp3"}},
				},
			},
			dir: "testdata/artist/album2",
			p:   1.0,
			s:   0,
			c: []torrent.Change{
				{From: "ddd/file.mp3", To: "ddd/file6.mp3"},
				{From: "file4.mp3", To: "file5.mp3"},
			},
		},
		// torrent name doesn't match, files don't match, change dir and files
		// do NOT html decode on paths and file names
		{
			info: metainfo.Info{
				Length: 1340 + 1340,
				Name:   "info26&#52;34",
				Files: []metainfo.FileInfo{
					{Length: 1340, Path: []string{"file&#53;.mp3"}},
					{Length: 1340, Path: []string{"ddd", "file6.mp3"}},
				},
			},
			dir: "testdata/artist/album2",
			p:   1.0,
			s:   0,
			c: []torrent.Change{
				{From: "testdata/artist/album2", To: "testdata/artist/info26&#52;34"},
				{From: "file4.mp3", To: "file&#53;.mp3"},
				{From: "ddd/file.mp3", To: "ddd/file6.mp3"},
			},
		},
		// torrent name matches, file names match, but subdir doesn't match, change subdir name
		{
			info: metainfo.Info{
				Length: 1340 + 1340,
				Name:   "album2",
				Files: []metainfo.FileInfo{
					{Length: 1340, Path: []string{"file4.mp3"}},
					{Length: 1340, Path: []string{"eee", "file.mp3"}},
				},
			},
			dir: "testdata/artist/album2",
			p:   1.0,
			s:   0,
			c: []torrent.Change{
				{From: "ddd/file.mp3", To: "eee/file.mp3"},
			},
		},
		// torrent name matches, subdir matches, file name doesn't match, change file name
		{
			info: metainfo.Info{
				Length: 1340 + 1340,
				Name:   "album2",
				Files: []metainfo.FileInfo{
					{Length: 1340, Path: []string{"file4.mp3"}},
					{Length: 1340, Path: []string{"ddd", "file6.mp3"}},
				},
			},
			dir: "testdata/artist/album2",
			p:   1.0,
			s:   0,
			c: []torrent.Change{
				{From: "ddd/file.mp3", To: "ddd/file6.mp3"},
			},
		},
		// torrent name matches, subdir doesn't match file name doesn't match, change subdir and file
		{
			info: metainfo.Info{
				Length: 1340 + 1340,
				Name:   "album2",
				Files: []metainfo.FileInfo{
					{Length: 1340, Path: []string{"file4.mp3"}},
					{Length: 1340, Path: []string{"eee", "file1.mp3"}},
				},
			},
			dir: "testdata/artist/album2",
			p:   1.0,
			s:   0,
			c: []torrent.Change{
				{From: "ddd/file.mp3", To: "eee/file1.mp3"},
			},
		},
		// torrent name doesn't match, subdir matches file name matches, just change torrent dir
		{
			info: metainfo.Info{
				Length: 1340 + 1340,
				Name:   "info26438",
				Files: []metainfo.FileInfo{
					{Length: 1340, Path: []string{"file4.mp3"}},
					{Length: 1340, Path: []string{"ddd", "file.mp3"}},
				},
			},
			dir: "testdata/artist/album2",
			p:   1.0,
			s:   0,
			c: []torrent.Change{
				{From: "testdata/artist/album2", To: "testdata/artist/info26438"},
			},
		},
	}

	for _, td := range testData {
		p, s, c := torrent.CompareInfo(td.info, td.dir)
		if td.p != float64(p) && (td.p == 0 || math.Abs(td.p-float64(p))/td.p > 0.01) {
			t.Errorf("CompareInfo(%v, %s) failed: expected p %f, got %f (%f > 0.01)", td.info, td.dir, td.p, p, math.Abs(td.p-float64(p))/td.p)
		}
		if td.s != s {
			t.Errorf("CompareInfo(%v, %s) failed: expected s %d, got %d", td.info, td.dir, td.s, s)
		}
		if !equalChange(td.c, c) {
			t.Errorf("CompareInfo(%v, %s) failed: expected c %+v, got %+v", td.info, td.dir, td.c, c)
		}
	}
}

// Copyright © 2018 Charles Haynes <ceh@ceh.bz>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// TODO
// split out compare to take a torrent and a dir as a command
// cope when the search for artist/album/format returns too many results.
// maybe start by searching based on the directory name as a proxy for the album
// detect and report when the directory given doesn't exist
// handle multiple directories on the command line

package torrent

import (
	"bytes"
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"

	"github.com/anacrolix/torrent/metainfo"
	"github.com/charles-haynes/whatapi"
	"gitlab.com/ceh/upoad/dir"
)

var torrents = map[ArtistAlbumStruct][]ID{}

// ID is a checkable bittorent
type ID int // right now just the ID

// Finder returns a lists of IDs
type Finder interface {
	Search(url.Values, chan whatapi.Torrent)
	Spec(dir.Spec, chan whatapi.Torrent)
	FileName(string, chan whatapi.Torrent)
	SaveTorrentFile(whatapi.Torrent) (string, error)
}

// GazelleFinder implements a Finder for Gazelle trackers
type GazelleFinder struct {
	whatapi.WhatAPI
}

// NewGazelleFinder creates a new GazelleFinder instance with a url
// for the server, and a username and password
func NewGazelleFinder(url, user, pass string) (Finder, error) {
	wcd, err := whatapi.NewWhatAPI(url, "")
	if err != nil {
		return GazelleFinder{}, err
	}

	err = wcd.Login(user, pass)
	if err != nil {
		return GazelleFinder{}, err
	}
	return GazelleFinder{wcd}, nil
}

// File saves the torrent file for a Torrent
func (g GazelleFinder) SaveTorrentFile(t whatapi.Torrent) (string, error) {
	url, err := g.CreateDownloadURL(t.Torrent.ID)
	if err != nil {
		return "", err
	}

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	mi, err := metainfo.Load(bytes.NewReader(buf))
	if err != nil {
		return "", err
	}

	// Create the file
	path := fmt.Sprintf("%s.%s.torrent",
		html.UnescapeString(t.Torrent.FilePath),
		mi.HashInfoBytes().HexString()[:16])
	fmt.Printf("creating %s\n", path)
	out, err := os.Create(path)
	if err != nil {
		return "", err
	}
	defer out.Close()

	// Write the body to file
	n, err := out.Write(buf)
	if err != nil {
		return "", err
	}
	fmt.Printf("Wrote %d\n", n)

	return path, nil
}

// Search implements the Finder interface and sends IDs to a channel
// given a advanced search struct
func (g GazelleFinder) Search(v url.Values, ch chan whatapi.Torrent) {
	defer close(ch)
	torrentSearch, err := g.SearchTorrents("", v)
	if err != nil {
		log.Printf("Search(\"\",%q): %s", v, err)
		return
	}

	limit := 10
	for _, r := range torrentSearch.Results {
		for _, t := range r.Torrents {
			to, err := g.GetTorrent(t.TorrentID, url.Values{})
			if err != nil {
				log.Printf("GetTorrent(%d): %s", t.TorrentID, err)
				return
			}
			ch <- to
			limit--
			if limit == 0 {
				return
			}
		}
	}
}

// FileName implements the Finder interface and returns a slice of
// IDs given a file name in a torrent
func (g GazelleFinder) FileName(f string, ch chan whatapi.Torrent) {
	torrentSearchParams := url.Values{
		"filelist": {f},
	}
	g.Search(torrentSearchParams, ch)
}

// Spec implements the Finder interface and returns a slice of
// IDs given a torrent spec
func (g GazelleFinder) Spec(s dir.Spec, ch chan whatapi.Torrent) {
	torrentSearchParams := url.Values{
		"artistname": {s.Artist},
		"groupname":  {s.Album},
		"format":     {s.Format},
	}
	g.Search(torrentSearchParams, ch)
}

// ArtistAlbumStruct is a single album by a single artist
type ArtistAlbumStruct struct{ Artist, Album string }

// Change represents a rename that must be done to make the directory
// match the torrent
type Change struct{ From, To string }

func find(s string, sl []string) int {
	for i, v := range sl {
		if s == v {
			return i
		}
	}
	return -1
}

func compareDir(inv map[int64][]string, root string) (float32, int64, []Change) {
	c := []Change{}
	var totalSize int64
	for s, v := range inv {
		totalSize += s * int64(len(v))
	}
	if totalSize == 0 {
		return 1.0, 0, c
	}
	ch := make(chan dir.Entry)
	go dir.Enumerate(root, ch)
	for {
		td, ok := <-ch
		if !ok {
			break
		}
		if !td.Info.Mode().IsRegular() {
			continue
		}
		s, ok := inv[td.Info.Size()]
		if !ok || len(s) <= 0 {
			// there's no file with this size in the torrent
			continue
		}
		if i := find(td.Name, s); i >= 0 {
			// found name, size match. remove it from the list
			inv[td.Info.Size()] = append(s[:i], s[i+1:]...)
			continue
		}
		// there's no file of this size with that name in the torrent
		// throw away the last one, this works if there's only
		// one file, but not so well otherwise
		// TODO: check that the files are actually the same
		n := s[len(s)-1]
		f := filepath.Join(td.Dir, td.Name)
		r, err := filepath.Rel(root, f)
		if err != nil {
			return 0.0, 0, []Change{}
		}
		if r != n {
			c = append(c, Change{From: r, To: n})
		}
		inv[td.Info.Size()] = s[:len(s)-1]
	}
	missingSize := int64(0)
	for s, n := range inv {
		if len(n) > 0 {
			missingSize += int64(len(n)) * s
		}
	}
	p := 1.0 - float32(missingSize)/float32(totalSize)
	return p, missingSize, c
}

// CompareInfo compares a torrent *file* to a dir and returns a closeness metric
// ranging from 0.0 (no match at all) to 1.0 (identical) and a size
// difference saying how many bytes would need to be downloaded to make
// the match perfect
func CompareInfo(i metainfo.Info, root string) (float32, int64, []Change) {
	// compare the sizes of the files in the torrent to the files in the directory
	// if all the torrent files are there, return 1.0, 0
	// otherwise return 0.0, (sum of sizes of missing files)
	inv := map[int64][]string{}
	c := []Change{}
	torrentDir := i.Name
	dest := filepath.Join(filepath.Dir(root), torrentDir)
	if torrentDir != filepath.Base(root) {
		c = append(c, Change{From: root, To: dest})
	}
	for _, f := range i.Files {
		inv[f.Length] = append(inv[f.Length], filepath.Join(f.Path...))
	}
	p, s, c2 := compareDir(inv, root)
	return p, s, append(c, c2...)
}

// Compare compares a torrent *struct* to a dir and returns a closeness metric
// ranging from 0.0 (no match at all) to 1.0 (identical) and a size
// difference saying how many bytes would need to be downloaded to make
// the match perfect
func Compare(t whatapi.Torrent, root, dir string) (float32, int64, []Change) {
	// compare the sizes of the files in the torrent to the files in the directory
	// if all the torrent files are there, return 1.0, 0
	// otherwise return 0.0, (sum of sizes of missing files)
	inv := map[int64][]string{}
	c := []Change{}
	torrentDir := html.UnescapeString(t.Torrent.FilePath)
	if torrentDir != dir {
		c = append(c, Change{From: dir, To: torrentDir})
	}
	files, err := t.Torrent.Files()
	if err != nil {
		return 0.0, 0, []Change{}
	}
	for _, f := range files {
		inv[f.Size] = append(inv[f.Size], html.UnescapeString(f.Name))
	}

	p, s, c2 := compareDir(inv, filepath.Join(root, dir))
	return p, s, append(c, c2...)
}

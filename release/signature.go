// Copyright © 2018 Charles Haynes <ceh@ceh.bz>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package release

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/anacrolix/torrent/metainfo"
	"github.com/charles-haynes/whatapi"
)

type MSI = map[string]interface{}

// Signature represents a unique signature for an encoding of a release
// it consists of the type (cue, log, flac, mp3) and length in bytes
// of the log, cue and media files
type Signature struct {
	Cue   int64   // cue file length, 0 if none
	Log   int64   // log file length, 0 if none
	Type  string  // media file type, "flac" or "mp3"
	Files []int64 // lengths of the media files
}

//NewTorrentSignature returns a Signature for a torrent file
func NewTorrentSignature(f string) (Signature, error) {
	fmt.Printf("NewTorrentSignature: %s\n", f)
	s := Signature{}
	mi, err := metainfo.LoadFromFile(f)
	if err != nil {
		fmt.Printf("NewTorrentSignature: %s\n", err)
		return Signature{}, err
	}
	i, err := mi.UnmarshalInfo()
	if err != nil {
		fmt.Printf("NewTorrentSignature: %s\n", err)
		return Signature{}, err
	}
	for _, fi := range i.Files {
		name := fi.Path[len(fi.Path)-1]
		e := strings.ToLower(filepath.Ext(name))
		switch e {
		case ".cue":
			s.Cue = fi.Length
		case ".log":
			s.Log = fi.Length
		case ".flac", ".mp3":
			e := e[1:]
			if s.Type != "" && s.Type != e {
				return Signature{}, fmt.Errorf(
					"NewSignature: %s has both %s and %s",
					f, s.Type, e)
			}
			s.Type = e
			s.Files = append(s.Files, fi.Length)
		}
	}
	return s, nil
}

//NewWhatapiTorrentSignature returns a Signature for a whatapi torrent
func NewWhatapiTorrentSignature(t whatapi.Torrent) (Signature, error) {
	s := Signature{}
	f, err := t.Torrent.Files()
	if err != nil {
		return Signature{}, err
	}
	for _, fs := range f {
		e := strings.ToLower(filepath.Ext(fs.Name))
		switch e {
		case ".cue":
			s.Cue = fs.Size
		case ".log":
			s.Log = fs.Size
		case ".flac", ".mp3":
			e := e[1:]
			if s.Type != "" && s.Type != e {
				return Signature{}, fmt.Errorf(
					"NewSignature: %s has both %s and %s",
					t.Torrent.FilePath, s.Type, e)
			}
			s.Type = e
			s.Files = append(s.Files, fs.Size)
		}
	}
	return s, nil
}

//NewDirSignature returns a Signature for a directory
func NewDirSignature(d string) (Signature, error) {
	fmt.Printf("NewDirSignature: %s\n", d)
	s := Signature{}
	walkFn := func(path string, info os.FileInfo, err error) error {
		e := strings.ToLower(filepath.Ext(path))
		switch e {
		case ".cue":
			s.Cue = info.Size()
		case ".log":
			s.Log = info.Size()
		case ".flac", ".mp3":
			e := e[1:]
			if s.Type != "" && s.Type != e {
				return fmt.Errorf(
					"NewSignature: %s has both %s and %s",
					d, s.Type, e)
			}
			s.Type = e
			s.Files = append(s.Files, info.Size())
		}
		return nil
	}
	if err := filepath.Walk(d, walkFn); err != nil {
		return Signature{}, err
	}
	return s, nil
}

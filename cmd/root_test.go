// Copyright © 2018 Charles Haynes <ceh@ceh.bz>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd_test

import (
	"reflect"
	"testing"

	"github.com/anacrolix/torrent/metainfo"
	"gitlab.com/ceh/seed/torrent"
)

func TestSeedDir(t *testing.T) {
	exp := []struct {
		d string
		f string
		e struct {
			p float32
			s int64
			c []torrent.Change
		}
	}{
		{
			d: "../torrent/testdata/artist/album",
			f: "../torrent/testdata/artistalbum2.torrent",
			e: struct {
				p float32
				s int64
				c []torrent.Change
			}{
				p: 0.5,
				s: 1340,
				c: []torrent.Change{
					{
						From: "../torrent/testdata/artist/album",
						To:   "../torrent/testdata/artist/album2",
					},
					{
						From: "file.mp3",
						To:   "file4.mp3",
					},
				},
			},
		},
		{
			d: "../torrent/testdata/artist/album2",
			f: "../torrent/testdata/artistalbum2.torrent",
			e: struct {
				p float32
				s int64
				c []torrent.Change
			}{
				p: 1.0,
				s: 0,
				c: []torrent.Change{},
			},
		},
	}
	for _, e := range exp {
		m, err := metainfo.LoadFromFile(e.f)
		if err != nil {
			t.Errorf("Seed: load error %s", err)
			break
		}
		i, err := m.UnmarshalInfo()
		if err != nil {
			t.Errorf("Seed: unmarshal error %s", err)
		}
		p, s, c := torrent.CompareInfo(i, e.d)
		r := struct {
			p float32
			s int64
			c []torrent.Change
		}{p, s, c}
		if !reflect.DeepEqual(e.e, r) {
			t.Errorf("expected CompareInfo(%s, %s) to be %+v, got %+v", e.f, e.d, e.e, r)
		}
	}
}

// Copyright © 2018 Charles Haynes <ceh@ceh.bz>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/ceh/upload/release"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "signature",
	Short: "generate the music signature for a directory",
	Long: `Given a directory, generate a signature for the
dmusic files in that directory.

Uses log and/or cue file and media (flac, mp3) files to generate a unique
signature for an encoding of a release`,
	Run: Signature,
}

// Signature generates a signature for a torrent file, or media directory
func Signature(cmd *cobra.Command, args []string) {
	sig := release.Signature{}
	if len(args) != 1 {
		fmt.Printf("expected a directory\n")
		return
	}
	f, err := os.Open(args[0])
	if err != nil {
		log.Printf("Signature: %s", err)
		return
	}
	fi, err := f.Stat()
	if err != nil {
		log.Printf("Signature: %s", err)
		return
	}
	if fi.IsDir() {
		sig, err = release.NewDirSignature(args[0])
	} else {
		sig, err = release.NewTorrentSignature(args[0])
	}
	if err != nil {
		log.Printf("Signature: %s", err)
		return
	}
	e := json.NewEncoder(os.Stdout)

	if err := e.Encode(sig); err != nil {
		log.Printf("Signature: %s", err)
		return
	}
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

var cfgFile string

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.signature.yaml)")
	rootCmd.PersistentFlags().String("url", "", "gazelle server URL. required. no default")
	rootCmd.PersistentFlags().StringP("user", "u", "", "user name for the gazelle server. required. no default")
	rootCmd.PersistentFlags().StringP("password", "p", "", "password for the gazelle server. required. no default")
	rootCmd.MarkFlagRequired("url")
	rootCmd.MarkFlagRequired("user")
	rootCmd.MarkFlagRequired("password")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().StringP("dir", "d", ".", "name of the directory to scan")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in current directory
		viper.AddConfigPath(".")
		// Search config in home directory
		viper.AddConfigPath(home)
		// Search config with name ".signature" (without extension).
		viper.SetConfigName(".signature")
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Printf("Using config file: %s\n", viper.ConfigFileUsed())
	}
}
